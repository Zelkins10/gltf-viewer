#version 330 // OpenGL 3.3

uniform vec3 uLightDirection;
uniform vec3 uLightIntensity;

in vec3 vViewSpacePosition;
in vec3 vViewSpaceNormal;
in vec2 vTexCoords;

out vec3 fColor;

void main()
{
  vec3 viewSpaceNormal = normalize(vViewSpaceNormal);
  fColor = uLightIntensity * dot(viewSpaceNormal, uLightDirection) / 3.14;
}